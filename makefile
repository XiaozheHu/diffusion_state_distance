#####################################################
# Last Modified 2020-03-17 -- 
####################################################

# Different Executable Programs
EXE = dsd_driver.ex
SRCFILE = dsd_driver

# Hazmat Directory
HAZDIR = /home/xiaozhehu/Work/Projects/HAZMATH/hazmath

# Suite Sparse Directory
SSDIR = #/usr/lib/x86_64-linux-gnu

# Machine Specific Compilers and Libraries
CC = gcc
FC = gfortran
CXX = g++
CFLAGS = -g
FFLAGS = -g -fno-second-underscore
LDFLAGS = -g

# Suite Sparse
SSINCLUDE = #-I$(SSDIR)/include
SSLIB = #-L$(SSDIR)/lib -lsuitesparseconfig -lcholmod -lamd -lcolamd -lccolamd -lcamd -lspqr -lumfpack -lamd -lcxsparse

# HAZMAT Files
LIB = $(HAZDIR)/lib
HAZINCLUDE = -I$(HAZDIR)/include
INCLUDE = $(SSINCLUDE) $(HAZINCLUDE)

# Source and Object Files
SRCDIR = .
SRC = $(SRCDIR)/$(SRCFILE).c
OBJ = $(SRCDIR)/$(SRCFILE).o
OBJS= $(OBJ) $(LIB)/libhazmath.a

# Include all other Libraries including UMFPACK, its dependencies, BLAS, and LAPACK
#LIBS = -lm -lblas -llapack -lgfortran $(SSLIB)
LIBS = -lm -lblas -llapack $(SSLIB)

.PHONY: all

all: $(EXE)

$(EXE): $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) $(INCLUDE) $(LIBS) -o $(EXE)

%.o:	%.c
	$(CC) $(INCLUDE) $(CFLAGS) -o $@ -c  $< $(PROFFLAG)

%.o:	%.f
	$(FC) $(INCLUDE) $(FFLAGS) -o $@ -c  $< $(PROFFLAG)

clean:
	rm -rf $(EXE) $(SRCDIR)/*.o *.dSYM
