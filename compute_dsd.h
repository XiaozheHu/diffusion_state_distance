/*! \file examples/diffusion_state_distance/compute_dsd.h
 *
 *  Created by Xiaozhe Hu on 1/3/20.
 *  Copyright 2020_HAZMATH__. All rights reserved.
 *
 * \brief This contains all the functions that compute diffusion state (distance)
 *
 *
 */

/*!
 * \fn compute_dsd_one_pair(dCSRmat *P, dvector *weighted_degree, const INT node_i, const INT node_j, linear_itsolver_param *itparam, AMG_param *amgparam, const INT norm_type)
 *
 * \brief compute distance between a pair of nodes
 *
 * \param P                pointer to the transition matrix
 * \param weighted_degree  pointer to the weighted degree
 * \param node_i           index of one node
 * \param node_j           index of another node
 * \param itparam          pointer to the parameters for iterative methods
 * \param amgparam         pointer to the parameters for AMG
 * \param norm_type        use what type of norm to compute the diffusion state distance
 *
 * \return dsd_ij          diffusion state distance between node i and node j
 *
 * \todo   generate normalized graph Laplacian without using adjacency matrix A and graph Laplacian L -- Xiaozhe
 *
 * \author Xiaozhe Hu
 * \date   01/01/2020
 *
 */
REAL compute_dsd_one_pair(dCSRmat *P,
                          dvector *weighted_degree,
                          const INT node_i,
                          const INT node_j,
                          linear_itsolver_param *itparam,
                          AMG_param *amgparam,
                          const INT norm_type)
{
  // local variables
  REAL dsd_ij = 0.0;
  INT n = weighted_degree->row;
  INT i;


  //------------------------------
  // prepare related data
  //------------------------------
  // get total weighted degree
  REAL total_weighted_degree = 0.0;
  for (i=0; i<n; i++) total_weighted_degree = total_weighted_degree + weighted_degree->val[i];

  // get adjacency Matrix
  dCSRmat A = get_adjacency_from_transition(P, weighted_degree);

  // get graph Laplacian
  dCSRmat L = get_graphLaplacian_from_adjacency(&A, weighted_degree);

  // get square root of the weighted degree
  dvector weighted_degree_half = dvec_create(n);
  for (i=0; i<n; i++) weighted_degree_half.val[i] = sqrt(weighted_degree->val[i]);

  // get inverse square root of the weighted degree
  dvector weighted_degree_half_inv = dvec_create(n);
  for (i=0; i<n; i++) weighted_degree_half_inv.val[i] = 1.0/sqrt(weighted_degree->val[i]);

  // get normalized graph Laplacian
  dCSRmat N = get_normalizedgraphLaplacian_from_L_wdeg_inv(&L, &weighted_degree_half_inv);

  // get null space vector of the normalized graph Laplacian
  dvector d = dvec_create(n);
  for (i=0; i<n; i++) d.val[i] = weighted_degree_half.val[i]/sqrt(total_weighted_degree);

  //------------------------------
  // compute the DSD
  //------------------------------
  // form right hand side
  dvector b = dvec_create(n);
  dvec_set(n, &b, 0.0);
  b.val[node_i] = weighted_degree_half_inv.val[node_i];
  b.val[node_j] = -weighted_degree_half_inv.val[node_j];

  // set solution
  dvector x = dvec_create (n);
  dvec_set(n, &x, 0.0);

  // solve
  /* Set Solver Parameters */
  INT solver_flag=-20;
  solver_flag = linear_solver_dcsr_krylov_amg(&N, &b, &x, itparam, amgparam);

  // make the solution orthogonal to d
  dvec_orthog(&x, &d);

  // get final vector
  for (i=0; i<n; i++) x.val[i] = weighted_degree_half.val[i]*x.val[i];

  // compute diffusion distance between node i and node j
  switch (norm_type) {
    case TWONORM:
      dsd_ij = dvec_norm2(&x);
      break;

    default:
      dsd_ij = dvec_norm1(&x);
  }

  //------------------------------
  // clean and return
  //------------------------------
  // clean
  dcsr_free(&A);
  dcsr_free(&L);
  dcsr_free(&N);

  dvec_free(&weighted_degree_half);
  dvec_free(&weighted_degree_half_inv);
  dvec_free(&d);
  dvec_free(&b);
  dvec_free(&x);

  // return
  return dsd_ij;
}


/*!
 * \fn dDENSEmat compute_diffusion_state(dCSRmat *P, dvector *weighted_degree, linear_itsolver_param *itparam, AMG_param *amgparam)
 *
 * \brief compute diffusion state X
 *
 * \param P                pointer to the transition matrix
 * \param weighted_degree  pointer to the weighted degree
 * \param itparam          pointer to the parameters for iterative methods
 * \param amgparam         pointer to the parameters for AMG
 *
 * \return X               diffusion state
 *
 * \todo   generate normalized graph Laplacian without using adjacency matrix A and graph Laplacian L -- Xiaozhe
 *
 * \author Xiaozhe Hu
 * \date   01/04/2020
 *
 */
dDENSEmat compute_diffusion_state(dCSRmat *P,
                                  dvector *weighted_degree,
                                  linear_itsolver_param *itparam,
                                  AMG_param *amgparam)
{
  // local variables
  INT n = weighted_degree->row;
  INT i,j;
  INT status = SUCCESS;

  // for AMG
  const SHORT max_levels = amgparam->max_levels;

  //------------------------------
  // prepare related data
  //------------------------------
  // diffusion state (dense matrix)
  dDENSEmat X = ddense_create(n,n);
  ddense_set(&X, 0.0);

  // get total weighted degree
  REAL total_weighted_degree = 0.0;
  for (i=0; i<n; i++) total_weighted_degree = total_weighted_degree + weighted_degree->val[i];

  // get adjacency Matrix
  dCSRmat A = get_adjacency_from_transition(P, weighted_degree);

  // get graph Laplacian
  dCSRmat L = get_graphLaplacian_from_adjacency(&A, weighted_degree);

  // get square root of the weighted degree
  dvector weighted_degree_half = dvec_create(n);
  for (i=0; i<n; i++) weighted_degree_half.val[i] = sqrt(weighted_degree->val[i]);

  // get inverse square root of the weighted degree
  dvector weighted_degree_half_inv = dvec_create(n);
  for (i=0; i<n; i++) weighted_degree_half_inv.val[i] = 1.0/sqrt(weighted_degree->val[i]);

  // get normalized graph Laplacian
  dCSRmat N = get_normalizedgraphLaplacian_from_L_wdeg_inv(&L, &weighted_degree_half_inv);

  // get null space vector of the normalized graph Laplacian
  dvector d = dvec_create(n);
  for (i=0; i<n; i++) d.val[i] = weighted_degree_half.val[i]/sqrt(total_weighted_degree);

  //------------------------------
  // setup AMG
  //------------------------------
  // number of nonzeros of normalized graph Laplacian N
  const INT nnz = N.nnz;

  // initialize A, b, x for mgl[0]
  AMG_data *mgl=amg_data_create(max_levels);
  mgl[0].A=dcsr_create(n,n,nnz); dcsr_cp(&N,&mgl[0].A);
  mgl[0].b=dvec_create(n); mgl[0].x=dvec_create(n);

  // setup AMG for normalized graph Laplacian
  status = amg_setup_ua(mgl, amgparam);
  if (status < 0) goto FINISHED;

  // setup preconditioner
  precond_data pcdata;
  param_amg_to_prec(&pcdata,amgparam);
  pcdata.max_levels = mgl[0].num_levels;
  pcdata.mgl_data = mgl;

  precond pc; pc.data = &pcdata;

  switch (amgparam->cycle_type) {

      case AMLI_CYCLE: // AMLI cycle
          pc.fct = precond_amli;
      break;

      case NL_AMLI_CYCLE: // Nonlinear AMLI AMG
          pc.fct = precond_nl_amli;
      break;

      default: // V,W-Cycle AMG
          pc.fct = precond_amg;
      break;

  }

  //------------------------------
  // compute diffusion state
  //------------------------------
  // right hand side b
  dvector b = dvec_create(n);

  // decomposition of b: b1 and b2
  dvector b1 = dvec_create(n);
  dvector b2 = dvec_create(n);
  // b2 = (1/sqrt(dtotal))*d
  dvec_cp(&d, &b2);
  dvec_ax(1./sqrt(total_weighted_degree), &b2);
  // b1 = -b2
  dvec_cp(&b2, &b1);
  dvec_ax(-1.0, &b1);

  // solution
  dvector x = dvec_create(n);

  // main loop
  for (i=0;i<n;i++){

    printf("i=%d\n",i);

    // b = D^{-1/2}*ei
    dvec_set(n, &b, 0.0);
    b.val[i] = weighted_degree_half_inv.val[i];

    // update b1
    if (i==0){
      b1.val[i] = b.val[i] + b1.val[i];
    }
    else {
      b1.val[i] = b.val[i] + b1.val[i];
      b1.val[i-1] = -b2.val[i-1];
    }

    // compute Nx = b1 (solve by AMG)
    dvec_set(n, &x, 0.0);
    status = solver_dcsr_linear_itsolver(&N, &b1, &x, &pc, itparam);

    // make the solution orthogonal to d
    dvec_orthog(&x, &d);

    // x = x + b2
    dvec_axpy(1.0, &b2, &x);

    // x = D^{1/2}*x
    for (j=0; j<n; j++) x.val[j] = weighted_degree_half.val[j]*x.val[j];

    // put it in X
    // diffusion state is stored row-wise, each row corresponds to the diffusion state of one node
    array_cp(n, x.val, &(X.val[i*n]));

    //array_print(&(X.val[i*n]), 20);
    //getchar();

  }

FINISHED:
  //------------------------------
  // clean and return
  //------------------------------
  // clean
  dcsr_free(&A);
  dcsr_free(&L);
  dcsr_free(&N);

  dvec_free(&weighted_degree_half);
  dvec_free(&weighted_degree_half_inv);
  dvec_free(&d);
  dvec_free(&b);
  dvec_free(&b1);
  dvec_free(&b2);
  dvec_free(&x);

  amg_data_free(mgl, amgparam);

  // return
  return X;
}


/*!
 * \fn dDENSEmat compute_approx_diffusion_state_JL(dCSRmat *P, dvector *weighted_degree, linear_itsolver_param *itparam, AMG_param *amgparam, const REAL epsilon)
 *
 * \brief compute approximate diffusion state X based on the Johnson-Lindenstrauss (JL) Lemma
 *
 * \param P                pointer to the transition matrix
 * \param weighted_degree  pointer to the weighted degree
 * \param itparam          pointer to the parameters for iterative methods
 * \param amgparam         pointer to the parameters for AMG
 * \param epsilon          tolerance for JL Lemma
 *
 * \return X               approximate diffusion state
 *
 * \todo   generate normalized graph Laplacian without using adjacency matrix A and graph Laplacian L -- Xiaozhe
 * \todo   implicitly generate JL random matrix -- Xiaozhe
 *
 * \author Xiaozhe Hu
 * \date   01/04/2020
 *
 */
dDENSEmat compute_approx_diffusion_state_JL(dCSRmat *P,
                                            dvector *weighted_degree,
                                            linear_itsolver_param *itparam,
                                            AMG_param *amgparam,
                                            REAL epsilon)
{
  // local variables
  INT n = weighted_degree->row;
  INT i,j,idx ;
  INT status = SUCCESS;

  // for AMG
  const SHORT max_levels = amgparam->max_levels;

  //------------------------------
  // prepare related data
  //------------------------------
  // get total weighted degree
  REAL total_weighted_degree = 0.0;
  for (i=0; i<n; i++) total_weighted_degree = total_weighted_degree + weighted_degree->val[i];

  // get adjacency Matrix
  dCSRmat A = get_adjacency_from_transition(P, weighted_degree);

  // get graph Laplacian
  dCSRmat L = get_graphLaplacian_from_adjacency(&A, weighted_degree);

  // get square root of the weighted degree
  dvector weighted_degree_half = dvec_create(n);
  for (i=0; i<n; i++) weighted_degree_half.val[i] = sqrt(weighted_degree->val[i]);

  // get inverse square root of the weighted degree
  dvector weighted_degree_half_inv = dvec_create(n);
  for (i=0; i<n; i++) weighted_degree_half_inv.val[i] = 1.0/sqrt(weighted_degree->val[i]);

  // get normalized graph Laplacian
  dCSRmat N = get_normalizedgraphLaplacian_from_L_wdeg_inv(&L, &weighted_degree_half_inv);

  // get null space vector of the normalized graph Laplacian
  dvector d = dvec_create(n);
  for (i=0; i<n; i++) d.val[i] = weighted_degree_half.val[i]/sqrt(total_weighted_degree);

  //------------------------------
  // setup AMG
  //------------------------------
  // number of nonzeros of normalized graph Laplacian N
  const INT nnz = N.nnz;

  // initialize A, b, x for mgl[0]
  AMG_data *mgl=amg_data_create(max_levels);
  mgl[0].A=dcsr_create(n,n,nnz); dcsr_cp(&N,&mgl[0].A);
  mgl[0].b=dvec_create(n); mgl[0].x=dvec_create(n);

  // setup AMG for normalized graph Laplacian
  status = amg_setup_ua(mgl, amgparam);
  if (status < 0) goto FINISHED;

  // setup preconditioner
  precond_data pcdata;
  param_amg_to_prec(&pcdata,amgparam);
  pcdata.max_levels = mgl[0].num_levels;
  pcdata.mgl_data = mgl;

  precond pc; pc.data = &pcdata;

  switch (amgparam->cycle_type) {

      case AMLI_CYCLE: // AMLI cycle
          pc.fct = precond_amli;
      break;

      case NL_AMLI_CYCLE: // Nonlinear AMLI AMG
          pc.fct = precond_nl_amli;
      break;

      default: // V,W-Cycle AMG
          pc.fct = precond_amg;
      break;

  }

  //------------------------------
  // prepare for JL lemma
  //------------------------------
  //const REAL epsilon = 0.5;  // accuracy of JL Lemma
  INT k = ceil((6.0/(pow(epsilon, 2)/2.0 - pow(epsilon, 3)/3.0))*log(n)); // low dimension

  // JL random matrix
  dDENSEmat Q = ddense_random_JL(k, n);

  // Q = Q*D^{1/2}
  for (i=0;i<k;i++){
    for (j=0;j<n;j++){
      idx = i*n+j;
      Q.val[idx] = Q.val[idx]*weighted_degree_half.val[j];
    }
  }

  // approximate diffusion state (dense matrix)
  dDENSEmat X = ddense_create(n,k);
  ddense_set(&X, 0.0);

  //------------------------------
  // compute approximate diffusion state
  //------------------------------
  // right hand side b
  dvector b = dvec_create(n);

  // solution
  dvector x = dvec_create(n);

  // main loop
  for (i=0;i<k;i++){

    //printf("i=%d\n",i);

    // b = Y(i,:)
    array_cp(n, &(Q.val[i*n]), b.val);

    // make b orthogonal to d
    dvec_orthog(&b, &d);

    // compute Nx = b (solve by AMG)
    dvec_set(n, &x, 0.0);
    status = solver_dcsr_linear_itsolver(&N, &b, &x, &pc, itparam);

    // make the solution orthogonal to d
    dvec_orthog(&x, &d);

    // x = D^{-1/2}*x
    for (j=0; j<n; j++) x.val[j] = weighted_degree_half_inv.val[j]*x.val[j];

    // put it in X
    // diffusion state is stored row-wise, each row corresponds to the approximate diffusion state of one node
    for (j=0; j<n; j++) X.val[j*k+i] = x.val[j];

  }

FINISHED:
  //------------------------------
  // clean and return
  //------------------------------
  // clean
  dcsr_free(&A);
  dcsr_free(&L);
  dcsr_free(&N);
  ddense_free(&Q);

  dvec_free(&weighted_degree_half);
  dvec_free(&weighted_degree_half_inv);
  dvec_free(&d);
  dvec_free(&b);
  dvec_free(&x);

  amg_data_free(mgl, amgparam);

  // return
  return X;
}
