/*! \file dsd_driver.c
 *
 *  Created by Xiaozhe Hu on 01/03/20.
 *  Copyright 2019_HAZMATH__. All rights reserved.
 *
 * \brief This program read in a PPI network and compute DSD or approximate DSD using the JL Lemma
 *
 * \note
 *
 */

/************* HAZMATH FUNCTIONS and INCLUDES ***************************/
#include "hazmath.h"
#include "compute_dsd.h"
/***********************************************************************/

/****** MAIN DRIVER **************************************************/
int main (int argc, char* argv[])
{

  printf("\n===========================================================================\n");
  printf("Beginning Program to compute diffusion state distance.\n");
  printf("===========================================================================\n");

  /* transition matrix and degree */
  dCSRmat P;  // transition matrix
  dvector weighted_degree; // weighted degree

  /*  type of norm used to compute DSD */
  INT norm_type = TWONORM; // ONENORM or TWONORM

  // local variable
  REAL compute_start, compute_end, compute_duration;

  // DSD related stuff
  dDENSEmat X;  // diffusion state
  dDENSEmat Xt; // approximate diffusion state

  dvector DSD_distance; // DSD
  dvector approx_DSD_distance; // approximate DSD

  REAL epsilon; //tolerance for JL Lemma

  // flags
  INT FLAG_USE_JL = 1;  // flag for using JL to approximate
  INT FLAG_WRITE_FILE = 0;   // flag for writing the (approximate) diffusion state to file (note: not the distance!! this is the coordinates)
  INT FLAG_COMPUTE_DSD = 1;  // flag for computing the (approximate) diffusion state distance

  printf("\n===========================================================================\n");
  printf("Reading the transition matrix, weighted degree, and parameters\n");
  printf("===========================================================================\n");

  /* read the transition matrix and degree */
  dcoo_read_dcsr("./PPI/worm/P.dat", &P);
  dvector_read("./PPI/worm/d.dat", &weighted_degree);

  /* set Parameters from Reading in Input File */
  input_param inparam;
  param_input_init(&inparam);
  param_input("./input.dat", &inparam);

  /* Set parameters for linear iterative methods */
  linear_itsolver_param linear_itparam;
  param_linear_solver_set(&linear_itparam, &inparam);

  /* Set parameters for algebriac multigrid methods */
  AMG_param amgparam;
  param_amg_init(&amgparam);
  param_amg_set(&amgparam, &inparam);
  //param_amg_print(&amgparam);

  /*
  printf("\n===========================================================================\n");
  printf("Compute the diffusion state distance between one pair of nodes\n");
  printf("=============================================================================\n");
  // compute DSD between one pair of nodes
  REAL dsd_ij;
  // index of node start from 0
  INT node_i = 1;
  INT node_j = 2;
  dsd_ij = compute_dsd_one_pair(&P, &weighted_degree, node_i, node_j, &linear_itparam, &amgparam, norm_type);

  // output the distance
  //printf("dsd = %f\n", dsd_ij);
  */

  if (FLAG_USE_JL == 0)
  {
    printf("\n===========================================================================\n");
    printf("Compute the diffusion state\n");
    printf("===========================================================================\n");
    // diffusion state is stored row-wise: each row stores the diffusion state of one node
    get_time(&compute_start);
    X = compute_diffusion_state(&P, &weighted_degree, &linear_itparam, &amgparam);
    get_time(&compute_end);
    compute_duration = compute_end - compute_start;
    print_cputime("Compute the diffusion state", compute_duration);

    if (FLAG_WRITE_FILE == 1)
    {
      //write the diffusion state on file
      printf("\n===========================================================================\n");
      printf("Write the diffusion state on file\n");
      printf("===========================================================================\n");
      ddense_write("X.dat", &X);
    }


    if (FLAG_COMPUTE_DSD == 1)
    {
      printf("\n===========================================================================\n");
      printf("Compute DSD\n");
      printf("===========================================================================\n");
      get_time(&compute_start);
      DSD_distance = pairwise_distance(&X, norm_type);
      get_time(&compute_end);
      compute_duration = compute_end - compute_start;
      print_cputime("Compute DSD", compute_duration);
      //array_print(DSD_distance.val, 20);
    }
  }
  else
  {
    printf("\n===========================================================================\n");
    printf("Compute the approximate diffusion state via JL Lemma\n");
    printf("===========================================================================\n");
    // diffusion state is stored row-wise: each row stores the approximate diffusion state of one node
    epsilon = 0.5;
    get_time(&compute_start);
    Xt = compute_approx_diffusion_state_JL(&P, &weighted_degree, &linear_itparam, &amgparam, epsilon);
    get_time(&compute_end);
    compute_duration = compute_end - compute_start;
    print_cputime("Compute the approximate diffusion state", compute_duration);

    if (FLAG_WRITE_FILE == 1)
    {
      printf("\n===========================================================================\n");
      printf("Write the approximate diffusion state on file\n");
      printf("===========================================================================\n");
      ddense_write("Xt.dat", &Xt);
    }

    if (FLAG_COMPUTE_DSD)
    {
      printf("\n===========================================================================\n");
      printf("Compute approximate DSD\n");
      printf("===========================================================================\n");
      get_time(&compute_start);
      approx_DSD_distance = pairwise_distance(&Xt, norm_type);
      get_time(&compute_end);
      compute_duration = compute_end - compute_start;
      print_cputime("Compute approximate DSD", compute_duration);
      //array_print(approx_DSD_distance.val, 20);
    }

  }

  printf("\n===========================================================================\n");
  printf("Clean up memory \n");
  printf("===========================================================================\n");
  // Clean up memory
  dcsr_free(&P);
  dvec_free(&weighted_degree);

  if (FLAG_USE_JL == 0)
  {
    ddense_free(&X);
    dvec_free(&DSD_distance);
  }
  else
  {
    ddense_free(&Xt);
    dvec_free(&approx_DSD_distance);
  }

  // done !!
  printf("\n=========================================================================\n");
  printf("Finishing Program to compute diffusion state distance.\n");
  printf("=========================================================================\n");

}	/* End of Program */
/*******************************************************************/
